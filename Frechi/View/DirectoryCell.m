//
//  DirectoryCell.m
//  Frechi
//
//  Created by master on 9/1/16.
//  Copyright © 2016 master. All rights reserved.
//

#import "DirectoryCell.h"
#import "CommonUtils.h"
#import "UserEntity.h"


@implementation DirectoryCell

@synthesize imvMarket , lblTitle , lblDescription;
- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (void) setUserModel:(UserEntity *) _entity {
    
 
    [imvMarket setImageWithURL:[NSURL URLWithString:_entity._itemImageUrl]];
    lblTitle.text = _entity._title;
    lblDescription.text = _entity._description;
      
}

@end
