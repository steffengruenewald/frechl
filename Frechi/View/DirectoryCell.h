//
//  DirectoryCell.h
//  Frechi
//
//  Created by master on 9/1/16.
//  Copyright © 2016 master. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserEntity.h"


@interface DirectoryCell : UITableViewCell{
    
}

@property (weak, nonatomic) IBOutlet UIImageView *imvMarket;

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;

@property (weak, nonatomic) IBOutlet UILabel *lblDescription;


- (void) setUserModel:(UserEntity *) _entity;

@end
