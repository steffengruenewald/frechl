//
//  AppDelegate.h
//  Frechi
//
//  Created by master on 8/29/16.
//  Copyright © 2016 master. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserEntity.h"
#import "PCAngularActivityIndicatorView.h"

@import GoogleMobileAds;


@interface AppDelegate : UIResponder <UIApplicationDelegate>{
    
      UIView* baseView;     
}

@property (strong, nonatomic) UIWindow  * _Nonnull window;

- (void) showAlertDialog :(NSString * _Nullable)title message:(NSString * _Nullable) message positive:(NSString * _Nullable)strPositivie negative:(NSString * _Nullable) strNegative sender:(id _Nullable) sender;

@property (nonatomic, strong) UserEntity * _Nullable Me;
@property (nonnull , strong) NSMutableArray *MyItems;
@property (nonatomic, strong) PCAngularActivityIndicatorView *_Nullable activityIndicator;
@property (nonatomic) int fromPush;

- (void) showLoadingView:(id _Nullable) sender;
- (void) hideLoadingView;
- (void) hideLoadingView: (NSTimeInterval) delay;

@end

