//
//  AppDelegate.m
//  Frechi
//
//  Created by master on 8/29/16.
//  Copyright © 2016 master. All rights reserved.
//

#import "AppDelegate.h"
#import "UserDefault.h"
//#import <FacebookSDK/FacebookSDK.h>
@import Firebase;
@import FirebaseInstanceID;
@import FirebaseMessaging;


@interface AppDelegate ()
{
    
}
@end

@implementation AppDelegate
@synthesize Me , MyItems, fromPush;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    [self initBaseView];
     Me = [[UserEntity alloc] init];
    MyItems = [[NSMutableArray alloc] init];
    fromPush = 0;
    [GADMobileAds configureWithApplicationID:@"ca-app-pub-3940256099942544/2934735716"];
    
    
    //Setting push notifications to ios9.3.5
    UIUserNotificationType allNotificationTypes =
    (UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge);
    UIUserNotificationSettings *settings =
    [UIUserNotificationSettings settingsForTypes:allNotificationTypes categories:nil];
    [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
    
    [[UIApplication sharedApplication] registerForRemoteNotifications];
    
    [FIRApp configure];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(tokenRefreshNotification:) name:kFIRInstanceIDTokenRefreshNotification object:nil];
    
   // }
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    [[FIRMessaging messaging] disconnect];
    NSLog(@"Disconnected from FCM");
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    [self connectToFcm];
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (void) showLoadingView:(id) sender
{
    //UIViewController * _supver = (UIViewController *)sender;
    
    [[[[UIApplication sharedApplication] delegate] window] addSubview:baseView];
   
    self.activityIndicator = [[PCAngularActivityIndicatorView alloc] initWithActivityIndicatorStyle:PCAngularActivityIndicatorViewStyleDefault];
    self.activityIndicator.color = [UIColor whiteColor];
    [_window addSubview:self.activityIndicator];
    self.activityIndicator.center = [[[UIApplication sharedApplication] delegate] window].center;
    
    [self.activityIndicator startAnimating];
}

- (void) hideLoadingView {
    
    [baseView removeFromSuperview];
    [self.activityIndicator stopAnimating];
}

////Message process
-(void) showAlertDialog:(NSString *)title message:(NSString *)message positive:(NSString *)strPositivie negative:(NSString *)strNegative sender:(id)sender
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    if(strPositivie != nil)
    {
        UIAlertAction * yesButton = [UIAlertAction
                                     actionWithTitle:strPositivie
                                     style:UIAlertActionStyleDefault
                                     handler:^(UIAlertAction * action)
                                     {
                                         //Handel your yes please button action here
                                         [self finishedAlert];
                                         [alert dismissViewControllerAnimated:YES completion:nil];
                                         
                                     }];
        
        [alert addAction:yesButton];
    }
    if(strNegative != nil) {
        UIAlertAction * noButton = [UIAlertAction
                                    actionWithTitle:strPositivie
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action)
                                    {
                                        //Handel your yes please button action here
                                        
                                        [alert dismissViewControllerAnimated:YES completion:nil];
                                    }];
        
        [alert addAction:noButton];
    }
    
    [sender presentViewController:alert animated:YES completion:nil];
    alert.view.tintColor = [UIColor colorWithRed:15/255.0 green:108/255.0 blue:36/255.0 alpha:1.0];
}

//view for prevent touch event when loading
- (void) initBaseView {
    baseView = [[UIView alloc] initWithFrame:CGRectMake(0,
                                                        0,
                                                        [[UIScreen mainScreen] applicationFrame].size.width,
                                                        [[UIScreen mainScreen] applicationFrame].size.height)];
    
    [baseView setBackgroundColor:[UIColor blackColor]];
    baseView.userInteractionEnabled = YES;
    baseView.alpha = 0.05;
}

//Firebase settings


- (void)tokenRefreshNotification:(NSNotification *)notification {
    // Note that this callback will be fired everytime a new token is generated, including the first
    // time. So if you need to retrieve the token as soon as it is available this is where that
    // should be done.
    NSString *refreshedToken = [[FIRInstanceID instanceID] token];
    NSLog(@"InstanceID token: %@", refreshedToken);
    
    [UserDefault setStringValue:@"token" value:refreshedToken];
    if(Me._idx > 0)
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"RegisterToken" object:self userInfo:nil];
    }
    
    // Connect to FCM since connection may have failed when attempted before having a token.
    [self connectToFcm];
    
    // TODO: If necessary send token to application server.
}


// With "FirebaseAppDelegateProxyEnabled": NO
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    NSLog(@"token : %@", deviceToken);
    [[FIRInstanceID instanceID] setAPNSToken:deviceToken type:FIRInstanceIDAPNSTokenTypeSandbox];
}

- (void)connectToFcm {
    [[FIRMessaging messaging] connectWithCompletion:^(NSError * _Nullable error) {
        if (error != nil) {
            NSLog(@"Unable to connect to FCM. %@", error);
        } else {
            NSLog(@"Connected to FCM.");
        }
    }];
}

// [START receive_message]

-(void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler{
    //if (userInfo.getboll)
    fromPush = FROM_REQUESTACCEPTED;
    if(application.applicationState == UIApplicationStateActive) {
     
        
    }else if(application.applicationState == UIApplicationStateBackground){
        
        //app is in background, if content-available key of your notification is set to 1, poll to your backend to retrieve data and update your interface here
        
    }else if(application.applicationState == UIApplicationStateInactive){
        fromPush = 3;
        
        //app is transitioning from background to foreground (user taps notification), do what you need when user taps here
    }
}

- (UIViewController *)topViewController{
    return [self topViewController:[UIApplication sharedApplication].keyWindow.rootViewController];
}

- (UIViewController *)topViewController:(UIViewController *)rootViewController
{
    if (rootViewController.presentedViewController == nil) {
        return rootViewController;
    }
    
    if ([rootViewController.presentedViewController isKindOfClass:[UINavigationController class]]) {
        UINavigationController *navigationController = (UINavigationController *)rootViewController.presentedViewController;
        UIViewController *lastViewController = [[navigationController viewControllers] lastObject];
        return [self topViewController:lastViewController];
    }
    
    UIViewController *presentedViewController = (UIViewController *)rootViewController.presentedViewController;
    return [self topViewController:presentedViewController];
}

-(void) finishedAlert
{
    
}

-(void) gotoMakeDeal{
    //if()
}
@end
