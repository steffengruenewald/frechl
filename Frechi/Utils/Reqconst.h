//
//  Reqconst.h
//  Frechi
//
//  Created by master on 8/31/16.
//  Copyright © 2016 master. All rights reserved.
//

#ifndef Reqconst_h
#define Reqconst_h

#define SERVER_BASE_URL                 @"http://52.37.29.212"

#define SERVER_URL [NSString stringWithFormat:@"%@%@", SERVER_BASE_URL, @"/index.php/api/"]



#define REQ_LOGIN                       @"login"
#define REQ_REGISTER                    @"register"
#define REQ_UPDATEPROFILE               @"updateProfile"
#define REQ_UPLOADPHOTO                 @"uploadImage"


#define RES_CODE                        @"result_code"

#define REQ_GETALLITEMS                 @"getAllItems"
#define REQ_MTITEM                      @"getMyItems"
#define REQ_POSTITEM                    @"postitem"
#define REQ_DELETEITEM                  @"deleteItem"
//#define REQ_CHANGEITEM                  @"changeItem"
#define REQ_SENDCHANGE                  @"sendChangePush"
#define REQ_SEARCHSCHOOL                @"searchSchool"


#define RES_ID                          @"userId"
#define RES_USERINFO                    @"userInfos"
#define RES_USERNAME                    @"userName"
#define RES_EMAIL                       @"email"
#define RES_PHOTOURL                    @"userPhotoUrl"
#define ALL_ITEMS                       @"allItems"
#define RES_ITEMID                      @"itemId"
#define RES_TITLE                       @"title" 
#define RES_DESCRIPTION                 @"description"
#define RES_USERPHOTOURL                @"userPhotoUrl"
#define RES_ITEMIMAGEURL                @"itemImageUrl"
#define RES_MYITEMS                     @"myItems"
#define RES_ITEMIMAGE_URL               @"photoUrl"
#define RES_SCHOOLNAME                  @"schoolName"

#define RES_INFORMATIONS                @"informations"


#define PARAM_USERNAME                   @"userName"
#define PARAM_SCHOOLNAME                 @"schoolName"
#define PARAM_SOY                        @"soy"
#define PARAM_EGG                        @"egg"
#define PARAM_DAIRY                      @"dairy"
#define PARAM_FISH                       @"fish"
#define PARAM_SHELLFISH                  @"sellfish"
#define PARAM_WHEAT                      @"wheat"
#define PARAM_TREE                       @"tree"
#define PARAM_PEANUT                     @"peanut"
#define PARAM_USERID                     @"userId"
#define PARAM_FILE                       @"file"
#define PARAM_TITLE                      @"title"
#define PARAM_DESCRIPTION                @"description"

#define CODE_SUCCESS                    0
#define CODE_UNREGUSER                  100
#endif /* Reqconst_h */
