//
//  UserDefault.h
//  Frechi
//
//  Created by master on 9/1/16.
//  Copyright © 2016 master. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Constant.h"
#import "PrefConst.h"
#import "AppDelegate.h"

@interface UserDefault : NSObject

// set and get method with string
+(void) setStringValue:(NSString *) keyString value:(NSString*) value;
+(NSString *) getStringValue:(NSString *) keyString;

@end
