//
//  CommonUtils.h
//  Frechi
//
//  Created by master on 8/31/16.
//  Copyright © 2016 master. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Constant.h"
#import "Reqconst.h"
#import "UserDefault.h"
#import "AppDelegate.h"




@interface CommonUtils : NSObject
+(NSString * _Nullable) saveToFile:(UIImage *_Nullable) scrImage;
+ (UIImage *) imageResize: (UIImage *_Nullable) srcImage resizeTo:(CGSize) newSize;
+(NSString * _Nullable) saveToFile:(UIImage *) scrImage unique:(NSString *)itemString;


+ (void) setUserName: (NSString *) userName;

+ (NSString * _Nullable) getUserName;

+ (void) setLastLoginUserName:(NSString *) _username;

+ (NSString *) getLastLoginUserName;

+ (long) changedToggleValue : (long)selectedValue status : (long *) toggleStatus;
@end
