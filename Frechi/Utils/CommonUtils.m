//
//  CommonUtils.m
//  Frechi
//
//  Created by master on 8/31/16.
//  Copyright © 2016 master. All rights reserved.
//

#import "CommonUtils.h"
#import "UIImage+ResizeMagick.h"

@implementation CommonUtils

+ (UIImage *) imageResize: (UIImage *) srcImage resizeTo:(CGSize) newSize {
    
    UIImage * resImage;
    
    resImage = [srcImage resizedImageByMagick:@"128x128#"];
    return resImage;
}



// save image to file (Documents/Boneservice/profile.png
+ (NSString *) saveToFile:(UIImage *) srcImage unique:(NSString *)itemString {
    
    NSString * savedPhotoURL = nil;
    
    NSString * outputFileName = @"";
    UIImage * outputImage;
    
    NSLog(@"scr width = %f, scr height = %f", srcImage.size.width, srcImage.size.height);
    
    // set output file name and resize source image with output image size
    outputFileName = @"profile.jpg";
    outputImage = [CommonUtils imageResize:srcImage resizeTo:CGSizeMake(PROFILE_IMG_SIZE, PROFILE_IMG_SIZE)];
    
    NSLog(@"out width = %f, out height = %f", outputImage.size.width, outputImage.size.height);
    
    NSFileManager * fileManager = [NSFileManager defaultManager];
    NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString * documentsDirectory = [paths objectAtIndex:0];
    
    // current document directory
    [fileManager changeCurrentDirectoryPath:documentsDirectory];
    [fileManager createDirectoryAtPath:[NSString stringWithFormat:@"%@%@",SAVE_ROOT_PATH,itemString] withIntermediateDirectories:YES attributes:nil error:NULL];
    
    // Documents/Campus
    documentsDirectory = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@%@",SAVE_ROOT_PATH,itemString]];
    NSString * filePath = [documentsDirectory stringByAppendingPathComponent:outputFileName];
    
    NSLog(@"save filePath = %@", filePath);
    
    // if the file exists already, delete and write, else if create filePath
    if([fileManager fileExistsAtPath:filePath]) {
        [fileManager removeItemAtPath:filePath error:nil];
    } else {
        [fileManager createFileAtPath:filePath contents:nil attributes:nil];
    }
    
    // Write a UIImage to PNG file
    //    [UIImagePNGRepresentation(outputImage) writeToFile:filePath atomically:YES];
    
    [UIImageJPEGRepresentation(outputImage, 1.0) writeToFile:filePath atomically:YES];
    
    NSError * error;
    [fileManager contentsOfDirectoryAtPath:documentsDirectory error:&error];
    
    return savedPhotoURL = filePath;
}



+ (void) setUserName: (NSString *) userName {
    
    [UserDefault setStringValue:PREFKEY_USERNAME value:userName];
}

+ (NSString *) getUserName {
    
    return [UserDefault getStringValue:PREFKEY_USERNAME];
}

+ (NSString *) getLastLoginUserName {
    
    return [UserDefault getStringValue:PREFKEY_LASTLOGINID];
}

+ (void) setLastLoginUserName:(NSString *) _username {
    
    [UserDefault setStringValue:PREFKEY_LASTLOGINID value:_username];
}


+ (long) changedToggleValue : (long)selectedValue status : (long *) toggleStatus
{
    NSLog(@"%ld,%ld", selectedValue, *toggleStatus);
    
    if (*toggleStatus & selectedValue)
    {
        *toggleStatus -= selectedValue;
        return 1;
    }
    else
    {
        *toggleStatus += selectedValue;
        return 0;
  
    }
}

@end
