//
//  Constant.h
//  Frechi
//
//  Created by master on 8/31/16.
//  Copyright © 2016 master. All rights reserved.
//

#import <Foundation/Foundation.h>

@import AFNetworking;


@interface Constant : NSObject

/**
 **     Userdefaults and AppDelegate Macro
 **/
#pragma mark -
#pragma mark - UserDefaults and AppDelegate

//#define USERDEFAULTS [NSUserDefaults standardUserDefaults]

#define USERDEFAULTS [NSUserDefaults standardUserDefaults]
#define APPDELEGATE ((AppDelegate*)[[UIApplication sharedApplication] delegate])


// -------------------------------------------------------------
// string define
// -------------------------------------------------------------

#define   ALERT_OK                  @"Okay"
#define   INPUT_USERNAME            @"Please input your username"
#define   INPUT_PASSWORD            @"Please input your password"
#define   INPUT_EMAIL               @"Please input your email"
#define   UNREGISTERED_USER         @"Unregistered user."
#define READ_TERMS                  @"Please read and accept the Terms and Condition."
#define   CONN_ERROR                @"Connecting to the server failed.\nPlease try again."
#define PROFILE_IMG_SIZE 256
#define SAVE_ROOT_PATH                          @"Frachi"
#define   PHOTO_UPLOAD_FAIL          @"Picture registration failed."
#define   PHOTO_UPLOAD_SUCCESS      @"The uploaded file was successful."


#define     FROM_REQUESTRECEIVED    1
#define     FROM_REQUESTACCEPTED    2
@end
