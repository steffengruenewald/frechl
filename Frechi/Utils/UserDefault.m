//
//  UserDefault.m
//  Frechi
//
//  Created by master on 9/1/16.
//  Copyright © 2016 master. All rights reserved.
//

#import "UserDefault.h"

@implementation UserDefault

-(id) init {
    
    self = [super init];
    if(self) {
        // custom init code
    }
    
    return self;
}

// set and get method with string
+(void) setStringValue:(NSString *) keyString value:(NSString*) value {
    
    [USERDEFAULTS setValue:value forKey:keyString];
}

+(NSString *) getStringValue:(NSString *) keyString {
    
    return [USERDEFAULTS valueForKey:keyString];
}
@end
