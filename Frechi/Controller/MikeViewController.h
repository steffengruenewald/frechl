//
//  MikeViewController.h
//  Frechi
//
//  Created by master on 9/11/16.
//  Copyright © 2016 master. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ViewController.h"

@interface MikeViewController : UIViewController{
    
    
}

@property(nonatomic , strong) NSString* myImageURL;

@property(nonatomic , strong) NSString* userImageURL;

@property(nonatomic , strong) ViewController* rootVC;

-(void) updateState;


@end
