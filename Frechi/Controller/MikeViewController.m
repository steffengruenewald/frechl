//
//  MikeViewController.m
//  Frechi
//
//  Created by master on 9/11/16.
//  Copyright © 2016 master. All rights reserved.
//

#import "MikeViewController.h"
#import "ViewController.h"
#import "AppDelegate.h"
#import "CommonUtils.h"


@interface MikeViewController(){
    
    
}
@property (weak, nonatomic) IBOutlet UIImageView *imvMyItem;

@property (weak, nonatomic) IBOutlet UIImageView *imvUserItem;

@end

@implementation MikeViewController

-(void) viewDidLoad{
    
    [super viewDidLoad];
    
    [self initView];
}

-(void) viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
}


-(void) initView{
    
    _imvMyItem.layer.borderColor = [[UIColor darkGrayColor] CGColor];
    _imvMyItem.layer.borderWidth = 1;
    
    _imvUserItem.layer.borderColor = [[UIColor darkGrayColor] CGColor];
    _imvUserItem.layer.borderWidth = 1;

}

-(void) updateState{
    
   // [_imvMyItem setImageWithURL:[NSURL URLWithString:_myImageURL]];
    
    [_imvMyItem setImageWithURL:[NSURL URLWithString:_myImageURL]];
    [_imvUserItem setImageWithURL:[NSURL URLWithString:_userImageURL]];
        
}

- (IBAction)gotoCongrate:(id)sender {
    [APPDELEGATE hideLoadingView];
    [self.rootVC gotoCongrate];
}

- (IBAction)gotoDashBoard:(id)sender {
    [APPDELEGATE hideLoadingView];
    [self.rootVC gotoDashBoard];
}
@end
