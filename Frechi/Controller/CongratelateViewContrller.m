//
//  CongratelateViewContrller.m
//  Frechi
//
//  Created by master on 9/18/16.
//  Copyright © 2016 master. All rights reserved.
//

#import "CongratelateViewContrller.h"
#import "ViewController.h"
#import "AppDelegate.h"
#import "CommonUtils.h"


@interface CongratelateViewContrller(){
    
    
}
@property (weak, nonatomic) IBOutlet UIImageView *imvMyItemImage;

@property (weak, nonatomic) IBOutlet UIImageView *imvuserItemImage;
@end

@implementation CongratelateViewContrller


-(void) viewDidLoad{
    
    [super viewDidLoad];
    
    [self initView];
}

-(void) viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
}

-(void) initView{
    
    _imvMyItemImage.layer.borderWidth = 1;
    _imvMyItemImage.layer.borderColor = [[UIColor darkGrayColor] CGColor];
    _imvMyItemImage.layer.masksToBounds = YES;
    
    _imvuserItemImage.layer.borderWidth = 1;
    _imvuserItemImage.layer.borderColor = [[UIColor darkGrayColor] CGColor];
    _imvuserItemImage.layer.masksToBounds = YES;
        
}

-(void) updateState{
    
    [_imvMyItemImage setImageWithURL:[NSURL URLWithString:_myImageURL]];
    [_imvuserItemImage setImageWithURL:[NSURL URLWithString:_userImageURL]];
    
}

- (IBAction)gotoDashBoard:(id)sender {
    
    [self.rootVC gotoDashBoard];
}

    
@end
