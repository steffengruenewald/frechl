//
//  MyItemViewController.h
//  Frechi
//
//  Created by master on 9/5/16.
//  Copyright © 2016 master. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ViewController.h"

@interface MyItemViewController : UIViewController

@property (nonatomic, strong) ViewController * rootVC;

- (void) getMyItem;

@end
