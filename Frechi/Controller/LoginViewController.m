//
//  LoginViewController.m
//  Frechi
//
//  Created by master on 8/31/16.
//  Copyright © 2016 master. All rights reserved.
//

#import "LoginViewController.h"
#import "AppDelegate.h"
#import "CommonUtils.h"
#import "UserEntity.h"
//#import <FacebookSDK/FacebookSDK.h>



//@import  AFNetworking;

@interface LoginViewController ()<UITextFieldDelegate>{
    
    UserEntity *_user;
    NSString *_username;
}

@property (weak, nonatomic) IBOutlet UITextField *txtUserName;


@end

@implementation LoginViewController

-(void) viewDidLoad {
    
    [super viewDidLoad];
    
    _user = APPDELEGATE.Me;
    NSString *pref_Name = [CommonUtils getUserName];
    
    
    //check the validation
    if(pref_Name.length > 0) {

        _username = pref_Name;
        [self doLogin];
    }
    
}

-(void) viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];  

}

-(void)didReceiveMemoryWarning{
    
    [super didReceiveMemoryWarning];
}
- (IBAction)doLogin:(id)sender {
    if (_txtUserName.text.length == 0) {
        [self performSegueWithIdentifier:@"SequeLoginToCategory" sender:nil];
    }
  
    if([self checkValid]){
        _username = [_txtUserName.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        [self doLogin];
    }


}

-(BOOL) checkValid{
    
    if(_txtUserName.text.length == 0){
        return NO;
    }
    
    return YES;
}

-(void) doLogin{
    
    [APPDELEGATE showLoadingView:self];
    
    NSString *url = [NSString stringWithFormat:@"%@%@/%@", SERVER_URL, REQ_LOGIN,_username];
    
    NSLog(@"%@",url);
    
    NSString *  escapedURL = [url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    AFHTTPSessionManager * manager = [AFHTTPSessionManager manager];
    [manager GET:escapedURL parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        // success response
        // parse the data from server
        int nResult_Code = [[responseObject valueForKey:RES_CODE] intValue];
        if(nResult_Code == CODE_SUCCESS) {
            
             NSLog(@"%d",nResult_Code);
            NSDictionary * _dict = [responseObject objectForKey:RES_USERINFO];
            _user._idx = [[_dict valueForKey:RES_ID] intValue];
            _user._userName = [_dict valueForKeyPath:RES_USERNAME];
            _user._photoUrl = [_dict valueForKeyPath:RES_PHOTOURL];
            _user._schoolName = [_dict valueForKeyPath:RES_SCHOOLNAME];
            long temp = 255;
            [CommonUtils changedToggleValue:[[_dict valueForKey:PARAM_SOY] intValue]^1 status: &temp];
            [CommonUtils changedToggleValue:([[_dict valueForKey:PARAM_SHELLFISH] intValue]^1)*2 status: &temp];
            [CommonUtils changedToggleValue:([[_dict valueForKey:PARAM_EGG] intValue]^1)*4 status: &temp];
            [CommonUtils changedToggleValue:([[_dict valueForKey:PARAM_WHEAT] intValue]^1)*8 status: &temp];
            [CommonUtils changedToggleValue:([[_dict valueForKey:PARAM_DAIRY] intValue]^1)*16 status: &temp];
            [CommonUtils changedToggleValue:([[_dict valueForKey:PARAM_TREE] intValue]^1)*32 status: &temp];
            [CommonUtils changedToggleValue:([[_dict valueForKey:PARAM_FISH] intValue]^1)*64 status: &temp];
            [CommonUtils changedToggleValue:([[_dict valueForKey:PARAM_PEANUT] intValue]^1)*128 status: &temp];
            
            NSLog(@"%ld", temp);
        
            _user._toggleStatus = temp;
            
            [CommonUtils setUserName:_username];
            
            // init database if new user login
            if([CommonUtils getLastLoginUserName] == nil || ![[CommonUtils getLastLoginUserName] isEqualToString:_username]) {
                [CommonUtils setLastLoginUserName:_username];
                
                 [self performSegueWithIdentifier:@"SequeLoginToCategory" sender:nil];
                
            }
            
            [self performSegueWithIdentifier:@"SequeLoginToCategory" sender:nil];
           
            
        }
        else if(nResult_Code == CODE_UNREGUSER)
        {
            [APPDELEGATE hideLoadingView];
             [APPDELEGATE showAlertDialog:nil message:UNREGISTERED_USER positive:ALERT_OK negative:nil sender:self];
            
              [self performSegueWithIdentifier:@"SequeLoginToCategory" sender:nil];
            
            
        }
        
        
        
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        // failure
        [APPDELEGATE hideLoadingView];
        NSLog(@"Error: %@", error);
 
        //  [APPDELEGATE hideLoadingView];
        [APPDELEGATE showAlertDialog:nil message:CONN_ERROR positive:ALERT_OK negative:nil sender:self];
    }];

    
}
#pragma mark - UITextField Delegate

- (BOOL) textFieldShouldReturn:(UITextField *)textField
{
    

    [textField resignFirstResponder];
    return YES;
}

- (void) textFieldDidBeginEditing:(UITextField *)textField {
    
    if(_txtUserName == textField) {
        [UIView animateWithDuration:0.3 animations:^{
            CGRect f = self.view.frame;
            f.origin.y -= 150;//(offsetY + deltaY);
            self.view.frame = f;
        }];
    }
}

- (void) textFieldDidEndEditing:(UITextField *)textField {
    
    if (_txtUserName == textField) {
        
        [UIView animateWithDuration:0.3 animations:^{
            CGRect f = self.view.frame;
            f.origin.y = 0.0;
            self.view.frame = f;
        }];
    } 
}

-(void) touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}

- (IBAction)facebookLogin:(id)sender {
    
    
}

@end
