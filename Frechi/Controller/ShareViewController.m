//
//  ShareViewController.m
//  Frechi
//
//  Created by master on 8/31/16.
//  Copyright © 2016 master. All rights reserved.
//

#import "ShareViewController.h"
#import "UITextView+Placeholder.h"
#import "MessageUI/MessageUI.h"
#import <MessageUI/MFMailComposeViewController.h>


@interface ShareViewController()<UITextFieldDelegate, UITextViewDelegate , MFMailComposeViewControllerDelegate>{
    
}


@property (weak, nonatomic) IBOutlet UITextField *txtSubject;

@property (weak, nonatomic) IBOutlet UIView *vUsername;

@property (weak, nonatomic) IBOutlet UIView *vDescription;

@property (weak, nonatomic) IBOutlet UITextView *txtViewDescription;
@end

@implementation ShareViewController

- (void) viewDidLoad{
    
    [super viewDidLoad];
    [self initView];
}

- (void) viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
}

- (void) viewWillDisappear:(BOOL)animated{
    
    [super viewWillDisappear:animated];
}

- (void) initView{
    
    _vUsername.layer.cornerRadius = 5;
    _vUsername.layer.borderWidth = 1;
    _vUsername.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    _vUsername.layer.masksToBounds = YES;
    
    _vDescription.layer.cornerRadius = 5;
    _vDescription.layer.borderWidth = 1;
    _vDescription.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    _vDescription.layer.masksToBounds = YES;
    
    NSAttributedString *str = [[NSAttributedString alloc] initWithString:@"Name" attributes:@{ NSForegroundColorAttributeName : [UIColor darkGrayColor] ,/* NSFontAttributeName : [UIFont fontWithName:@"" size:<#(CGFloat)#>]*/}];
    
    _txtSubject.attributedPlaceholder = str;
    
   _txtViewDescription.placeholder = @"Message";
    _txtViewDescription.placeholderColor = [UIColor darkGrayColor];
    
}

#pragma mark - UITextField Delegate

- (BOOL) textFieldShouldReturn:(UITextField *)textField
{

    [textField resignFirstResponder];
    return YES;
}


- (void) textFieldDidBeginEditing:(UITextField *)textField {
    
    if(_txtSubject == textField) {
        [UIView animateWithDuration:0.3 animations:^{
            CGRect f = self.view.frame;
            f.origin.y -= 150;//(offsetY + deltaY);
            self.view.frame = f;
        }];
    }
}

- (void) textFieldDidEndEditing:(UITextField *)textField {
    
    if (_txtSubject == textField) {
        
        [UIView animateWithDuration:0.3 animations:^{
            CGRect f = self.view.frame;
            f.origin.y = 0.0;
            self.view.frame = f;
        }];
    }
 }

#pragma mark - UITextView Delegate


- (void) textViewDidBeginEditing:(UITextView *)textView{
    
    if(_txtViewDescription == textView) {
        [UIView animateWithDuration:0.3 animations:^{
            CGRect f = self.view.frame;
            f.origin.y -= 150;//(offsetY + deltaY);
            self.view.frame = f;
        }];
    }
}

- (void) textViewDidEndEditing:(UITextView *)textView{
    
    if (_txtViewDescription == textView) {
        
        [UIView animateWithDuration:0.3 animations:^{
            CGRect f = self.view.frame;
            f.origin.y = 0.0;
            self.view.frame = f;
        }];
    }
}

-(void) touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}
- (IBAction)sendMail:(id)sender {
    
    [self.view endEditing:YES];
    NSString *emailTitle = _txtSubject.text;
    NSString *messageBody = _txtViewDescription.text;
    // To address
    NSArray *toRecipents = [NSArray arrayWithObject:@"mobilestar1127@gmail.com"];
    
    MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
    mc.mailComposeDelegate = self;
    [mc setSubject:emailTitle];
    [mc setMessageBody:messageBody isHTML:NO];
    [mc setToRecipients:toRecipents];
    
    // Present mail view controller on screen
    [self presentViewController:mc animated:YES completion:NULL];
    
}

- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    // Close the Mail Interface
//     [self dismissViewControllerAnimated:YES completion:NULL];
    [self dismissViewControllerAnimated:YES completion:NULL];
}

@end
