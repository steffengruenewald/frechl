//
//  MyItemViewController.m
//  Frechi
//
//  Created by master on 9/5/16.
//  Copyright © 2016 master. All rights reserved.
//

#import "MyItemViewController.h"
#import "DirectoryCell.h"
#import "AppDelegate.h"
#import "CommonUtils.h"
#import "UserEntity.h"


@interface MyItemViewController()<UITableViewDelegate , UITableViewDataSource, GoToMyItemViewControllerDelegate> {
   
    NSMutableArray *myItemList;    
}

@property (weak, nonatomic) IBOutlet UITableView *tblMyItemList;

@end



@implementation MyItemViewController

@synthesize rootVC;

- (void) viewDidLoad{
    
    [super viewDidLoad];
    
    rootVC.delegate = self;    
}

- (void) reloadMyItemViewController{
    
    [self getMyItem];
}

- (void) viewWillAppear:(BOOL)animated{
    myItemList = [[NSMutableArray alloc] init];
    
    myItemList = APPDELEGATE.MyItems;
    [super viewWillAppear: animated];
    [self initView];
}

- (void) viewWillDisappear:(BOOL)animated{
    
    [super viewWillDisappear:animated];
}

- (void) initView{
    
    [self getMyItem];
}

- (void) getMyItem{
    
    if (APPDELEGATE.Me._idx > 0)
    {
       [APPDELEGATE showLoadingView:self];
        myItemList = [[NSMutableArray alloc] init];
        
        NSString *url = [NSString stringWithFormat:@"%@%@/%d", SERVER_URL, REQ_MTITEM,APPDELEGATE.Me._idx];
        NSLog(@"%d" , APPDELEGATE.Me._idx);
        NSLog(@"%@",url);
        
        NSString *  escapedURL = [url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        AFHTTPSessionManager * manager = [AFHTTPSessionManager manager];
        [manager GET:escapedURL parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            
            [APPDELEGATE hideLoadingView];
            // success response
            // parse the data from server
            
            int nResult_Code = [[responseObject valueForKey:RES_CODE] intValue];
            if(nResult_Code == CODE_SUCCESS) {
                
                NSLog(@"%d",nResult_Code);
                //UserEntity *itemEntity = [[UserEntity alloc] init];
                NSArray *jsonItems = [responseObject objectForKey:RES_MYITEMS];
                for(NSDictionary * _dict in jsonItems){
                    
                    UserEntity *itemEntity = [[UserEntity alloc] init];
                    itemEntity._itemId = [[_dict valueForKey:RES_ITEMID] intValue];
                    itemEntity._title = [_dict valueForKey:RES_TITLE];
                    itemEntity._description = [_dict valueForKey:RES_DESCRIPTION];
                    itemEntity._itemImageUrl =  [_dict valueForKey:RES_ITEMIMAGE_URL];
                    [myItemList addObject:itemEntity];
                    NSLog(@"%lu" , (unsigned long)myItemList.count);
                }
                
                
                [self.tblMyItemList reloadData];
                
            }
            else if(nResult_Code == CODE_UNREGUSER)
            {
                
                // [APPDELEGATE showAlertDialog:nil message:UNREGISTERED_USER positive:ALERT_OK negative:nil sender:self];
                
                // [self performSegueWithIdentifier:@"SequeLoginToCategory" sender:nil];
                
            }
            
            
            
            
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            
            // failure
            NSLog(@"Error: %@", error);
            
             [APPDELEGATE hideLoadingView];
            //[APPDELEGATE showAlertDialog:nil message:CONN_ERROR positive:ALERT_OK negative:nil sender:self];
        }];
    }
}


#pragma mark - UITableViewDataSource
// ---------------------------------------------------------------------------
- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView {
    
    // return the number of sections
    return 1;
}


- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return myItemList.count;
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    //DirectoryCell * cell = (DirectoryCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    static NSString * cellIdentifier =   @"DirectoryCell";
    DirectoryCell *cell = (DirectoryCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    [cell setUserModel:myItemList[indexPath.row]];
    
    return cell;
    
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 110;
}



///////*********longClick event**********///////

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}


- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [APPDELEGATE showLoadingView:self];
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        //remove the deleted object from your data source.
        //If your data source is an NSMutableArray, do this
        if (APPDELEGATE.Me._idx >0 )
        {
        
            UserEntity *deleteEntity = [[UserEntity alloc] init];
            
            deleteEntity = (UserEntity *)myItemList[indexPath.row];
            NSString *url = [NSString stringWithFormat:@"%@%@/%d", SERVER_URL, REQ_DELETEITEM, deleteEntity._itemId];
            
            NSString *  escapedURL = [url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            AFHTTPSessionManager * manager = [AFHTTPSessionManager manager];
            [manager GET:escapedURL parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                
                // success response
                // parse the data from server
                int nResult_Code = [[responseObject valueForKey:RES_CODE] intValue];
                if(nResult_Code == CODE_SUCCESS) {
                   
                    [self getMyItem];
                    
                    
                }
                else if(nResult_Code == CODE_UNREGUSER)
                {
                    
                    // [APPDELEGATE showAlertDialog:nil message:UNREGISTERED_USER positive:ALERT_OK negative:nil sender:self];
                    
                    // [self performSegueWithIdentifier:@"SequeLoginToCategory" sender:nil];
                 }
                
                
                
                
            } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                
                // failure
                
                 [APPDELEGATE hideLoadingView];
                //[APPDELEGATE showAlertDialog:nil message:CONN_ERROR positive:ALERT_OK negative:nil sender:self];
            }];
            
            [myItemList removeObjectAtIndex:indexPath.row];
            [tableView reloadData];
            
         }
    }
}
//////////***************//////////////

- (IBAction)handleLongPress:(UILongPressGestureRecognizer *)sender {
    
    if(sender.state == UIGestureRecognizerStateBegan){
        
        CGPoint p = [sender locationInView:self.tblMyItemList];
        
        NSIndexPath *indexpath = [self.tblMyItemList indexPathForRowAtPoint:p];
        
        if(indexpath == nil){
            
           NSLog(@"long press on table view but not on a row");
        } else{
            
            UITableViewCell *cell = [self.tblMyItemList cellForRowAtIndexPath:indexpath];
            
            if(cell.isHighlighted){
                NSLog(@"long press on table view at section %ld row %ld", (long)indexpath.section, (long)indexpath.row);
            }
        }
    
    }
}







@end
