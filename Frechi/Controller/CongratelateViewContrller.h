//
//  CongratelateViewContrller.h
//  Frechi
//
//  Created by master on 9/18/16.
//  Copyright © 2016 master. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ViewController.h"


@interface CongratelateViewContrller : UIViewController

@property(nonatomic , strong) NSString *myImageURL;

@property(nonatomic , strong) NSString *userImageURL;

@property(nonatomic , strong) ViewController *rootVC;

-(void) updateState;

@end
