//
//  ViewController.m
//  Frechi
//
//  Created by master on 8/29/16.
//  Copyright © 2016 master. All rights reserved.
//

#import "ViewController.h"
#import "DashBoardViewController.h"
#import "LoginViewController.h"
#import "CommonUtils.h"
#import "MarketViewController.h"
#import "MakeDealViewController.h"
#import "MyItemViewController.h"
#import "AppDelegate.h"
#import "MikeViewController.h"
#import "CongratelateViewContrller.h"
@import GoogleMobileAds;




@interface ViewController () <GADBannerViewDelegate>{
    
    MakeDealViewController *makeDealVC;
    
    MikeViewController *mikeVC;
    
    CongratelateViewContrller *congrateVC;
    
    MyItemViewController *myItemVC;
    
    MarketViewController *marketVC;
  
    __weak IBOutlet NSLayoutConstraint *bannerViewHeight;
    
}



@property (weak, nonatomic) IBOutlet UIView *vSetting;

@property (weak, nonatomic) IBOutlet UIView *vInventory;

@property (weak, nonatomic) IBOutlet UIView *vDashBoard;

@property (weak, nonatomic) IBOutlet UIView *vMarket;

@property (weak, nonatomic) IBOutlet UIView *vMyItem;


@property (weak, nonatomic) IBOutlet UIView *vMakeDeal;

@property (weak, nonatomic) IBOutlet UIView *vInvite;


@property (weak, nonatomic) IBOutlet UIView *vMIke;

@property (weak, nonatomic) IBOutlet UIView *vCongrate;

@property (weak, nonatomic) IBOutlet GADBannerView *bannerView;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    //ADBannerView *asview
    
    self.bannerView.adUnitID  = @"ca-app-pub-8082809512496095/8657698162";
    self.bannerView.rootViewController = self;
    _bannerView.delegate = self;
    [self.bannerView loadRequest:[GADRequest request]];
    
    if(APPDELEGATE.Me._idx > 0){
       
        [self showContent:4];
    }
    else{
        [self showContent:3];
                
    }
    
    
    
}

-(void) viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear: animated];

}


// banner View delegate



-(void) adViewWillPresentScreen:(GADBannerView *)bannerView
{
    NSLog(@"banner view presented screen");
}

-(void) adViewDidReceiveAd:(GADBannerView *)bannerView
{
    bannerViewHeight.constant = 60;
      
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) viewWillAppear:(BOOL)animated{
    
    
    [super viewWillAppear:animated];
    
}

-(void) viewWillDisappear:(BOOL)animated{
    
    [super viewWillDisappear:animated];
}

/////*****status bar Hidden function*****/////

- (BOOL)prefersStatusBarHidden {
    return YES;
}

////////*******************//////////////
- (IBAction)showTrade:(id)sender {

    [self.view endEditing:YES];
    [self showContent:0];
}

- (IBAction)showInventory:(id)sender {
    
    [self.view endEditing:YES];
    [self showContent:1];
}

- (IBAction)showShare:(id)sender {
    
    [self.view endEditing:YES];
    [self showContent:2];
    
}

- (IBAction)showSetting:(id)sender {
    
    [self.view endEditing:YES];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"SetToggleStatus" object:self userInfo:nil];
    [self showContent:3];
}

- (IBAction)showDashBoard:(id)sender {

    [self.view endEditing:YES];
    [self showContent:4];
}


- (void) gotoMarket {
    
    [self reset];
    _vMarket.hidden = NO;
    [marketVC getAllItem];
    
}

- (void) gotoMyItem {
    
    [self showContent:1];
}

- (void) gotoMakeDeal{
    
    if (self.selectedImageURL != NULL) {
        
        makeDealVC.userImageUrl = self.selectedImageURL;
        makeDealVC.userName = self.selectedUserName;
        makeDealVC.userTitle = self.selectedTitle;
        makeDealVC.userItemId = self.selectItemId;
        makeDealVC.userId = self.selectedUserId;
        [makeDealVC updateState];
    }
    
    _vMakeDeal.hidden = NO;
    
    [makeDealVC getMyItem];
    
}

-(void) gotoMike{
    
    if(self.myItemImageURL != NULL){
        
        mikeVC.myImageURL = self.myItemImageURL;
        mikeVC.userImageURL = self.userItemImageURL;
        [mikeVC updateState];
    }
    
    [self reset];
    _vMIke.hidden = NO;
}

- (void) gotoInvite{
    
    //_vInvite.hidden = NO;
    [self showContent:2];
    
}

- (void) gotoInventory{
    
    [self showContent:0];
    
   // _vInventory.hidden = NO;
}

-(void) gotoCongrate{
    
    if(self.myItemImageURL != NULL){
        
        congrateVC.myImageURL = self.myItemImageURL;
        congrateVC.userImageURL = self.userItemImageURL;
        [congrateVC updateState];
    }
    [self reset];
    _vCongrate.hidden = NO;
}

-(void) gotoDashBoard{
    
    [self reset];
    _vDashBoard.hidden = NO;
}

- (void) showContent:(int) viewIndex {
    
    [self reset];
    
    switch (viewIndex) {
            
        case 0:
            
            _vInventory.hidden = NO;
            
            break;
            
        case 1:
            
            _vMyItem.hidden = NO;
            
            [_delegate reloadMyItemViewController];
            
            break;
            
        case 2:
            
            _vInvite.hidden = NO;
            break;
        case 3:
            
            _vSetting.hidden = NO;
            break;
            
        case 4:
           
            _vDashBoard.hidden = NO;
            break;
            
        default:
            break;
    }
    

}
-(void) reset{
    
    _vInventory.hidden = YES;
    _vSetting.hidden = YES;
    _vDashBoard.hidden = YES;
    _vMarket.hidden = YES;
    _vMyItem.hidden = YES;
    _vMakeDeal.hidden = YES;
    _vInvite.hidden = YES;
    _vMIke.hidden = YES;
    _vCongrate.hidden = YES;
    
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:@"SegueRoot2Dashboard"]) {

        DashBoardViewController *dashboardVC = (DashBoardViewController *)[segue destinationViewController];
        
        dashboardVC.rootVC = self;
        
    } else if ([segue.identifier isEqualToString:@"SegueRoot2MarketPlace"]) {
        
        marketVC = (MarketViewController *)[segue destinationViewController];
        marketVC.rootVC = self;
        
    } else if ([segue.identifier isEqualToString:@"SegueRoot2MakeDeal"]) {
        
        
        makeDealVC = (MakeDealViewController *)[segue destinationViewController];
        makeDealVC.rootVC = self;
        
        
    } else if ([segue.identifier isEqualToString:@"SegueRoot2Mike"]){
        
        mikeVC = (MikeViewController*)[segue destinationViewController];
        mikeVC.rootVC = self;
        
    } else if ([segue.identifier isEqualToString:@"SegueRoot2Congrate"]){
        
        congrateVC = (CongratelateViewContrller *)[segue destinationViewController];
        congrateVC.rootVC = self;
    } else if ([segue.identifier isEqualToString:@"SegueRoot2MyItem"]) {
        
        myItemVC = (MyItemViewController *) [segue destinationViewController];
        
        myItemVC.rootVC = self;
    }
}




@end
