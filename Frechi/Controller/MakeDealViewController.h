//
//  MakeDealViewController.h
//  Frechi
//
//  Created by master on 9/5/16.
//  Copyright © 2016 master. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ViewController.h"

@interface MakeDealViewController : UIViewController{
    
}
//@property(nonatomic, strong) NSIndexPath *selectedIndexPath;
@property(nonatomic , strong) NSString *userImageUrl;
@property(nonatomic , strong) NSString *userName;
@property(nonatomic , strong) NSString *userTitle;
@property (nonatomic) int  userId;
@property(nonatomic) int userItemId;

@property (nonatomic, strong) ViewController *rootVC;

-(void) updateState;

- (void) getMyItem;


@end
