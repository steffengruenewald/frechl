//
//  SettingViewController.m
//  Frechi
//
//  Created by master on 8/30/16.
//  Copyright © 2016 master. All rights reserved.
//

#import "SettingViewController.h"
#import "AppDelegate.h"
#import "CommonUtils.h"
#import "ActionSheetPicker.h"


@interface SettingViewController ()<UITextFieldDelegate, UIImagePickerControllerDelegate , UINavigationControllerDelegate>{
    
   // NSArray *state;
    NSString *_photoPath;

    NSMutableArray *imageArray;
    NSMutableArray *stateArray;
    NSArray * schoolNameArray;
    long toggleStatus;
    __weak IBOutlet UILabel *lblSchoolName;
    UserEntity * _user;
    
}

@property (weak, nonatomic) IBOutlet UIImageView *imvProfilePhoto;

@property (weak, nonatomic) IBOutlet UITextField *txtUserName;

@property (weak, nonatomic) IBOutlet UITextField *txtSearch;

@property (weak, nonatomic) IBOutlet UIView *VcontainSearch;

@property (weak, nonatomic) IBOutlet UIView *VcontainWSearch;

@end
@implementation SettingViewController

-(void) viewDidLoad {
    
    [super viewDidLoad];
    
    imageArray = [[NSMutableArray alloc] init];
    stateArray = [[NSMutableArray alloc] init];
    _user = APPDELEGATE.Me;
 
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setToggleItemStatus:) name:@"SetToggleStatus" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(registerToken:) name:@"RegisterToken" object:nil];
    [self initView];
    
}

-(void) initView{
    
    
    if(APPDELEGATE.Me._idx != 0 ){
       
        [_imvProfilePhoto setImageWithURL:[NSURL URLWithString:APPDELEGATE.Me._photoUrl]];
    }
    
    
    
    _imvProfilePhoto.layer.borderColor = [[UIColor redColor] CGColor];
    _imvProfilePhoto.layer.borderWidth = 1;
    
    NSAttributedString *str = [[NSAttributedString alloc] initWithString:@"Your Name" attributes:@{ NSForegroundColorAttributeName : [UIColor darkGrayColor] }];
    _txtUserName.attributedPlaceholder = str;
    
    str = [[NSAttributedString alloc] initWithString:@"Search by Zip or Gps" attributes:@{ NSForegroundColorAttributeName : [UIColor darkGrayColor] }];
    _txtSearch.attributedPlaceholder = str;
    
    /////***** View contain search cornerRadius setting ****//////
    
    _VcontainSearch.layer.cornerRadius = 10;
    _VcontainWSearch.layer.cornerRadius = 5;
    
    _txtUserName.text = _user._userName;
    lblSchoolName.text = _user._schoolName;
    
    [self setToggleStatus];
    
    NSLog(@"%ld", toggleStatus);

}

-(void) setToggleItemStatus: (NSNotification *)notification
{
    [self initView];
}
-(void) setToggleStatus{
    toggleStatus = _user._toggleStatus^255;
    for(int i = 1;i <= 130;i *=2)
    {
        [self setToggleImage:i];
    }
    toggleStatus = _user._toggleStatus;
}

-(void) viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
}

-(void) viewWillDisappear:(BOOL)animated{
    
    [super viewWillDisappear:animated];
}


- (IBAction)toggleImageTapped:(UITapGestureRecognizer *)sender {
    
    long selectedTag = sender.view.tag;
    [self setToggleImage:selectedTag];
}



- (void) setToggleImage:(long) tag{
    if([CommonUtils changedToggleValue:tag status: &toggleStatus] == 0)
    {
        ((UIImageView *)[self.view viewWithTag:(NSInteger)tag]).image = [UIImage imageNamed:@"toggle_on"];
    }
    else
    {
        ((UIImageView *)[self.view viewWithTag:(NSInteger)tag ]).image = [UIImage imageNamed:@"toggle_off"];
    }
}
- (IBAction)addPhoto:(id)sender {
    
    [self selectCamera];
}

-(void) selectCamera{
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Camera" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self openCamera];
        
    }]];
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Gallery" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self openGallery];
        //
    }]];
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }]];
    
    actionSheet.view.tintColor = [UIColor lightGrayColor];
    [self presentViewController:actionSheet animated:YES completion:nil];
}
-(void) openCamera
{
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        
        UIImagePickerController * imagePicker = [[UIImagePickerController alloc] init];
        imagePicker.delegate = self;
        imagePicker.allowsEditing = YES;
        imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        
        [self presentViewController:imagePicker animated:YES completion:nil];
        
    } else {
        
        NSLog(@"No Cameran\n. Please test on device");
    }
    
    
}
-(void) openGallery
{
    UIImagePickerController * imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.delegate = self;
    imagePicker.allowsEditing = YES;
    imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    [self presentViewController: imagePicker animated:YES completion:nil];
    
}
- (void) imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info {
    
    UIImage * chosenImage = info[UIImagePickerControllerEditedImage];
    
    // do some progress to scale with specified size and then save to local path
    // then set it to user profile photoPath
    [picker dismissViewControllerAnimated:YES completion:^{
        
        dispatch_queue_t writeQueue = dispatch_queue_create("SavePhoto", NULL);
        
        dispatch_async(writeQueue, ^{
            
            NSString * strPhotoPath = [CommonUtils saveToFile:chosenImage unique:@""];
            
            dispatch_async(dispatch_get_main_queue(), ^ {
                
                _photoPath = strPhotoPath;
  
                [self.imvProfilePhoto setImage:[UIImage imageWithContentsOfFile:strPhotoPath]];
                
            });
        });
    }];
}

- (void) imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - UITextField Delegate

- (BOOL) textFieldShouldReturn:(UITextField *)textField
{
    [self.view endEditing:YES];
    return YES;
}
- (IBAction)onFillZipCode:(UITextField *)sender {
    if (sender.text.length == 5)
    {
        [self.view endEditing:YES];
    }
}

- (void) textFieldDidBeginEditing:(UITextField *)textField {
    
    if(_txtSearch == textField) {
        [UIView animateWithDuration:0.3 animations:^{
            CGRect f = self.view.frame;
            f.origin.y -= 150;//(offsetY + deltaY);
            self.view.frame = f;
        }];
    }
}

- (void) textFieldDidEndEditing:(UITextField *)textField {
    
    if (_txtSearch == textField) {
        
        [UIView animateWithDuration:0.3 animations:^{
            CGRect f = self.view.frame;
            f.origin.y = 0.0;
            self.view.frame = f;
        }];
    }
}


-(void) touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}
- (IBAction)searchSchoolTapped:(id)sender {
    [self.view endEditing:YES];
    if (_txtSearch.text.length > 0){
        
        [self searchSchool: _txtSearch.text];
    }
    else
    {
        return;
    }
}

- (void) searchSchool: (NSString *) zipCode{
    [APPDELEGATE showLoadingView:self];
    
    NSString *url = [NSString stringWithFormat:@"%@%@/%@", SERVER_URL, REQ_SEARCHSCHOOL, zipCode];
   
    
    NSString *  escapedURL = [url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
     NSLog(@"%@",url);
    AFHTTPSessionManager * manager = [AFHTTPSessionManager manager];
    
    [manager GET:escapedURL parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        // success response
        // parse the data from server
        [APPDELEGATE hideLoadingView];
        int nResult_Code = [[responseObject valueForKey:RES_CODE] intValue];
        if(nResult_Code == CODE_SUCCESS) {
            
            NSLog(@"%d",nResult_Code);
             NSArray * _schools = [responseObject objectForKey:RES_SCHOOLNAME];
            
            
           [ActionSheetStringPicker showPickerWithTitle:@"Please select your school" rows:_schools initialSelection:0 doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
               
                lblSchoolName.text = _schools[selectedIndex];
                
            } cancelBlock:^(ActionSheetStringPicker *picker) {
                
            } origin:lblSchoolName];
            
            
        }
        else{
            lblSchoolName.text = @"Error occured!";
        }
        
        
        
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        // failure
        [APPDELEGATE hideLoadingView];
        NSLog(@"Error: %@", error);
        
        //  [APPDELEGATE hideLoadingView];
        [APPDELEGATE showAlertDialog:nil message:CONN_ERROR positive:ALERT_OK negative:nil sender:self];
    }];
}

- (void) registerUser {
    
   //set request url
    
    NSString * url = [NSString stringWithFormat:@"%@%@", SERVER_URL, REQ_REGISTER];    NSDictionary *params = @{
                             PARAM_USERNAME : _txtUserName.text,
                             PARAM_SCHOOLNAME : lblSchoolName.text,
                             PARAM_SOY : [NSString stringWithFormat:@"%ld", [CommonUtils changedToggleValue:1 status:&toggleStatus]],
                             PARAM_SHELLFISH : [NSString stringWithFormat:@"%ld", [CommonUtils changedToggleValue:2 status:&toggleStatus]],
                             PARAM_EGG : [NSString stringWithFormat:@"%ld", [CommonUtils changedToggleValue:4 status:&toggleStatus]],
                             PARAM_WHEAT : [NSString stringWithFormat:@"%ld", [CommonUtils changedToggleValue:8 status:&toggleStatus]],
                             PARAM_DAIRY : [NSString stringWithFormat:@"%ld", [CommonUtils changedToggleValue:16 status:&toggleStatus]],
                             PARAM_TREE : [NSString stringWithFormat:@"%ld", [CommonUtils changedToggleValue:32 status:&toggleStatus]],
                             PARAM_FISH : [NSString stringWithFormat:@"%ld", [CommonUtils changedToggleValue:64 status:&toggleStatus]],
                             PARAM_PEANUT : [NSString stringWithFormat:@"%ld", [CommonUtils changedToggleValue:128 status:&toggleStatus]]
                             };
    NSLog(@"%@, %@", url, params);
    NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] multipartFormRequestWithMethod:@"POST" URLString:url parameters:params constructingBodyWithBlock:^(id response) {}
         error:nil];
    
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    [APPDELEGATE showLoadingView:self];
    NSURLSessionUploadTask *uploadTask;
    uploadTask = [manager
                  uploadTaskWithStreamedRequest:request
                  progress:nil
                  completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
                      [APPDELEGATE hideLoadingView];
                      if (error) {
                          
                          NSLog(@"Error: %@", error);
                          
                          
                          [APPDELEGATE showAlertDialog:nil message:@"Register Failed!" positive:ALERT_OK negative:nil sender:self];
                          
                      } else {
                          
                          NSLog(@"%@ %@", response, responseObject);
                          
                          long nResultCode = [[responseObject valueForKey:RES_CODE] intValue];
                          
                          if(nResultCode == CODE_SUCCESS) {
                              
                              NSDictionary * userData = [responseObject objectForKey:RES_INFORMATIONS];
                              APPDELEGATE.Me._idx = [[userData valueForKey:RES_ID] intValue];
                              APPDELEGATE.Me._userName = _txtUserName.text;
                              APPDELEGATE.Me._schoolName = lblSchoolName.text;
                              APPDELEGATE.Me._toggleStatus = toggleStatus;
                              [CommonUtils setUserName:APPDELEGATE.Me._userName];
                              [[NSNotificationCenter defaultCenter] postNotificationName:@"RegisterToken" object:self userInfo:nil];
                              [self uploadPhoto];
                              
                          } else {
                              
                              [APPDELEGATE showAlertDialog:nil message:@"Register Failed!" positive:ALERT_OK negative:nil sender:self];
                          }
                      }
                  }];
    
    [uploadTask resume];//((UIImageView *)[self.view viewWithTag:(NSInteger)tag ]).image
}

- (void) updateProfile{
    //set request url
    
    NSString * url = [NSString stringWithFormat:@"%@%@", SERVER_URL, REQ_UPDATEPROFILE];
    //NSString *  escapedURL = [url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    //set parameters
    NSDictionary *params = @{
                             PARAM_USERID: [NSString stringWithFormat:@"%d",_user._idx],
                             PARAM_USERNAME : _txtUserName.text,
                             PARAM_SCHOOLNAME : lblSchoolName.text,
                             PARAM_SOY : [NSString stringWithFormat:@"%ld", [CommonUtils changedToggleValue:1 status:&toggleStatus]],
                             PARAM_SHELLFISH : [NSString stringWithFormat:@"%ld", [CommonUtils changedToggleValue:2 status:&toggleStatus]],
                             PARAM_EGG : [NSString stringWithFormat:@"%ld", [CommonUtils changedToggleValue:4 status:&toggleStatus]],
                             PARAM_WHEAT : [NSString stringWithFormat:@"%ld", [CommonUtils changedToggleValue:8 status:&toggleStatus]],
                             PARAM_DAIRY : [NSString stringWithFormat:@"%ld", [CommonUtils changedToggleValue:16 status:&toggleStatus]],
                             PARAM_TREE : [NSString stringWithFormat:@"%ld", [CommonUtils changedToggleValue:32 status:&toggleStatus]],
                             PARAM_FISH : [NSString stringWithFormat:@"%ld", [CommonUtils changedToggleValue:64 status:&toggleStatus]],
                             PARAM_PEANUT : [NSString stringWithFormat:@"%ld", [CommonUtils changedToggleValue:128 status:&toggleStatus]]
                             };
    NSLog(@"%@, %@", url, params);
    NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] multipartFormRequestWithMethod:@"POST" URLString:url parameters:params constructingBodyWithBlock:^(id response) {}
                                                                                                  error:nil];
    
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    [APPDELEGATE showLoadingView:self];
    NSURLSessionUploadTask *uploadTask;
    uploadTask = [manager
                  uploadTaskWithStreamedRequest:request
                  progress:nil
                  completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
                      [APPDELEGATE hideLoadingView];
                      if (error) {
                          
                          NSLog(@"Error: %@", error);
                          
                          
                          [APPDELEGATE showAlertDialog:nil message:@"Register Failed!" positive:ALERT_OK negative:nil sender:self];
                          
                      } else {
                          
                          NSLog(@"%@ %@", response, responseObject);
                          
                          long nResultCode = [[responseObject valueForKey:RES_CODE] intValue];
                          
                          if(nResultCode == CODE_SUCCESS) {
                              
                              NSDictionary * userData = [responseObject objectForKey:RES_INFORMATIONS];
                              APPDELEGATE.Me._idx = [[userData valueForKey:RES_ID] intValue];
                              APPDELEGATE.Me._userName = _txtUserName.text;
                              APPDELEGATE.Me._schoolName = lblSchoolName.text;
                              APPDELEGATE.Me._toggleStatus = toggleStatus;
                              [CommonUtils setUserName:APPDELEGATE.Me._userName];
                              
                              [self uploadPhoto];
                              
                          } else {
                              
                              [APPDELEGATE showAlertDialog:nil message:@"Register Failed!" positive:ALERT_OK negative:nil sender:self];
                          }
                      }
                  }];
    
    [uploadTask resume];//((UIImageView *)[self.view viewWithTag:
}

- (void) uploadPhoto{
    if (_photoPath.length == 0)
        return;
    
    NSString *url = [NSString stringWithFormat:@"%@%@", SERVER_URL, REQ_UPLOADPHOTO];
    NSDictionary *params = @{
                             @"userId": [NSString stringWithFormat: @"%d",APPDELEGATE.Me._idx ] };
    
    NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] multipartFormRequestWithMethod:@"POST" URLString:url parameters:params constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        [formData appendPartWithFileURL:[NSURL fileURLWithPath:_photoPath] name:@"file" fileName:@"filename.jpg" mimeType:@"image/jpg" error:nil];
    } error:nil];
    
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    [APPDELEGATE showLoadingView:self];
    NSURLSessionUploadTask *uploadTask;
    uploadTask = [manager
                  uploadTaskWithStreamedRequest:request
                  progress:nil
                  completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
                      [APPDELEGATE hideLoadingView];
                      if (error) {
                          
                          NSLog(@"Error: %@", error);
                          
                          
                          [APPDELEGATE showAlertDialog:nil message:PHOTO_UPLOAD_FAIL positive:ALERT_OK negative:nil sender:self];
                          
                      } else {
                          
                          NSLog(@"%@ %@", response, responseObject);
                          
                          int nResultCode = [[responseObject valueForKey:RES_CODE] intValue];
                          
                          if(nResultCode == 0) {
                              [APPDELEGATE showAlertDialog:nil message:@"Register Succeeded!" positive:ALERT_OK negative:nil sender:self];
                          } else {
                              [APPDELEGATE showAlertDialog:nil message:PHOTO_UPLOAD_FAIL positive:ALERT_OK negative:nil sender:self];
                          }
                      }
                  }];
    
    [uploadTask resume];
    
}

-(void) registerToken: (NSNotification *)notification
{
    
    if([UserDefault getStringValue:@"token"].length > 0){
    
        NSString *url = [NSString stringWithFormat:@"%@registerToken/%d/%@/1", SERVER_URL,APPDELEGATE.Me._idx,[UserDefault getStringValue:@"token"] ];
        
        NSLog(@"%@",url);
        
        NSString *  escapedURL = [url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        AFHTTPSessionManager * manager = [AFHTTPSessionManager manager];
        [manager GET:escapedURL parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            
            // success response
            // parse the data from server
            int nResult_Code = [[responseObject valueForKey:RES_CODE] intValue];
            if(nResult_Code == CODE_SUCCESS) {
                
                NSLog(@"Token register Succeeded");
            }
            else if(nResult_Code == CODE_UNREGUSER)
            {
                NSLog(@"Token register failed");
            }
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            
            // failure
            NSLog(@"Error");
        }];
    }
}

- (IBAction)saveProfileTapped:(id)sender {
    [self.view endEditing:YES];
    if(_txtUserName.text.length == 0 )
    {
        [APPDELEGATE showAlertDialog:nil message:@"Please input your username" positive:@"Okay" negative:nil sender:self];
        return;
    }
    if(lblSchoolName.text.length == 0)
    {
        [APPDELEGATE showAlertDialog:nil message:@"Please input your school name." positive:@"Okay" negative:nil sender:self];
        return;
    }
    if (APPDELEGATE.Me._idx > 0)
    {
        [self updateProfile];
    }
    else
        [self registerUser];
}


@end
