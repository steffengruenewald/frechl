//
//  InventoryViewController.m
//  Frechi
//
//  Created by master on 8/31/16.
//  Copyright © 2016 master. All rights reserved.
//

#import "InventoryViewController.h"
#import "UITextView+Placeholder.h"
#import "CommonUtils.h"


@interface InventoryViewController()<UITextViewDelegate , UITextViewDelegate , UIImagePickerControllerDelegate , UINavigationControllerDelegate,UITextFieldDelegate>{
    
    NSString *_photoPath3;
    NSString *_photoPath1;
    NSString *_photoPath2;
    
    NSString *select;
    
    NSMutableArray *arrPhotoPath;
    
    NSMutableArray *titleArray;
    
    NSMutableArray *descriptionArray;
    
}

///////item1///////
@property (weak, nonatomic) IBOutlet UIView *vTitle1;

@property (weak, nonatomic) IBOutlet UITextField *txtTitle1;

@property (weak, nonatomic) IBOutlet UIView *vDescription1;

@property (weak, nonatomic) IBOutlet UITextView *vtxtDescription1;
@property (weak, nonatomic) IBOutlet UIImageView *imvitem1;

/////****item2***////

@property (weak, nonatomic) IBOutlet UIView *vTitle2;

@property (weak, nonatomic) IBOutlet UITextField *txtTitle2;

@property (weak, nonatomic) IBOutlet UIView *vDescription2;

@property (weak, nonatomic) IBOutlet UITextView *vtxtDescription2;

@property (weak, nonatomic) IBOutlet UIImageView *imvitem2;
//////******Item3*********/////
@property (weak, nonatomic) IBOutlet UIView *vTitle3;

@property (weak, nonatomic) IBOutlet UITextField *txtTitle3;

@property (weak, nonatomic) IBOutlet UIView *vDescription3;

@property (weak, nonatomic) IBOutlet UITextView *vtxtDescription3;

@property (weak, nonatomic) IBOutlet UIImageView *imvitem3;

@end

@implementation InventoryViewController

-(void) viewDidLoad{
    
    [super viewDidLoad];
    
    titleArray = [[NSMutableArray alloc] init];
    
    descriptionArray = [[NSMutableArray alloc] init];
    
    [self initView];
    
    arrPhotoPath = [NSMutableArray array];
}

-(void) viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
}

-(void) viewWillDisappear:(BOOL)animated{
    
    [super viewWillDisappear:animated];
}

- (void) initView{
    
    _vTitle1.layer.cornerRadius = 5;
    _vTitle1.layer.borderWidth = 1;
    _vTitle1.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    _vTitle1.layer.masksToBounds = YES;
    
    _vDescription1.layer.cornerRadius = 5;
    _vDescription1.layer.borderWidth = 1;
    _vDescription1.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    _vDescription1.layer.masksToBounds = YES;
    
    _vTitle2.layer.cornerRadius = 5;
    _vTitle2.layer.borderWidth = 1;
    _vTitle2.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    _vTitle2.layer.masksToBounds = YES;
    
    _vDescription2.layer.cornerRadius = 5;
    _vDescription2.layer.borderWidth = 1;
    _vDescription2.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    _vDescription2.layer.masksToBounds = YES;
    
    _vTitle3.layer.cornerRadius = 5;
    _vTitle3.layer.borderWidth = 1;
    _vTitle3.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    _vTitle3.layer.masksToBounds = YES;
    
    _vDescription3.layer.cornerRadius = 5;
    _vDescription3.layer.borderWidth = 1;
    _vDescription3.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    _vDescription3.layer.masksToBounds = YES;
    
    ////////*********** imageview*********/////
    
    _imvitem1.layer.borderWidth = 1;
    _imvitem1.layer.borderColor = [UIColor darkGrayColor].CGColor;
    _imvitem1.layer.masksToBounds = YES;
    
    _imvitem2.layer.borderWidth = 1;
    _imvitem2.layer.borderColor = [UIColor darkGrayColor].CGColor;
    _imvitem2.layer.masksToBounds = YES;
    
    _imvitem3.layer.borderWidth = 1;
    _imvitem3.layer.borderColor = [UIColor darkGrayColor].CGColor;
    _imvitem3.layer.masksToBounds = YES;
    
    
}

#pragma mark - UITextField Delegate

- (BOOL) textFieldShouldReturn:(UITextField *)textField
{
    
    [textField resignFirstResponder];
    return YES;
}

- (void) textFieldDidBeginEditing:(UITextField *)textField {
    
    if(_txtTitle3 == textField) {
        [UIView animateWithDuration:0.3 animations:^{
            CGRect f = self.view.frame;
            f.origin.y -= 150;//(offsetY + deltaY);
            self.view.frame = f;
        }];
    }
    
}

- (void) textFieldDidEndEditing:(UITextField *)textField {
    
    if (_txtTitle3 == textField) {
        
        [UIView animateWithDuration:0.3 animations:^{
            CGRect f = self.view.frame;
            f.origin.y = 0.0;
            self.view.frame = f;
        }];
    }
}


#pragma mark - UITextView Delegate


- (void) textViewDidBeginEditing:(UITextView *)textView{
    
    if(_vtxtDescription3 == textView) {
        [UIView animateWithDuration:0.3 animations:^{
            CGRect f = self.view.frame;
            f.origin.y -= 150;//(offsetY + deltaY);
            self.view.frame = f;
        }];
    }
}

- (void) textViewDidEndEditing:(UITextView *)textView{
    
    if (_vtxtDescription3 == textView) {
        
        [UIView animateWithDuration:0.3 animations:^{
            CGRect f = self.view.frame;
            f.origin.y = 0.0;
            self.view.frame = f;
        }];
    }
}

-(void) touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}

- (IBAction)addPhotoItem1:(id)sender {
    [self.view endEditing:YES];
    
    select = @"item1";
    [self selectCamera];
}

- (IBAction)addPhotoItem2:(id)sender {
    [self.view endEditing:YES];
    
    select = @"item2";
    [self selectCamera];
}

- (IBAction)addPhotoItem3:(id)sender {
    [self.view endEditing:YES];
    
    select = @"item3";
    [self selectCamera];
}

-(void) selectCamera{
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Camera" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self openCamera];
        
    }]];
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Gallery" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self openGallery];
        //
    }]];
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }]];
    
    actionSheet.view.tintColor = [UIColor lightGrayColor];
    [self presentViewController:actionSheet animated:YES completion:nil];
}

-(void) openCamera
{
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        
        UIImagePickerController * imagePicker = [[UIImagePickerController alloc] init];
        imagePicker.delegate = self;
        imagePicker.allowsEditing = YES;
        imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        
        [self presentViewController:imagePicker animated:YES completion:nil];
        
    } else {
        
        NSLog(@"No Cameran\n. Please test on device");
    }
    
}

-(void) openGallery
{
    UIImagePickerController * imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.delegate = self;
    imagePicker.allowsEditing = YES;
    imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    [self presentViewController: imagePicker animated:YES completion:nil];
    
}

- (void) imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info {
    
    UIImage * chosenImage = info[UIImagePickerControllerEditedImage];
    
    // do some progress to scale with specified size and then save to local path
    // then set it to user profile photoPath
    [picker dismissViewControllerAnimated:YES completion:^{
        
        dispatch_queue_t writeQueue = dispatch_queue_create("SavePhoto", NULL);
        
        dispatch_async(writeQueue, ^{
            
            NSString * strPhotoPath = [CommonUtils saveToFile:chosenImage unique:select];
            
            dispatch_async(dispatch_get_main_queue(), ^ {
                
                
                
                // update ui (set profile image with saved Photo URL
                if([select  isEqual: @"item1"]){
                    _photoPath1 = strPhotoPath;
                    [self.imvitem1 setImage:[UIImage imageWithContentsOfFile:strPhotoPath]];
                    [arrPhotoPath insertObject:strPhotoPath atIndex:0];
                }
                
                else if ([select isEqual:@"item2"]){
                    
                    _photoPath2 = strPhotoPath;
                    [self.imvitem2 setImage:[UIImage imageWithContentsOfFile:strPhotoPath]];
                    [arrPhotoPath insertObject:strPhotoPath atIndex:1];
                }
                
                else if ([select isEqual:@"item3"]){
                    
                    _photoPath3 = strPhotoPath;
                    [self.imvitem3 setImage:[UIImage imageWithContentsOfFile:strPhotoPath]];
                    [arrPhotoPath insertObject:strPhotoPath atIndex:2];
                }
                //[self.imvProfile2 setImage:[UIImage imageWithContentsOfFile:strPhotoPath]];
            });
        });
    }];
}
- (void) imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)submitData:(id)sender {
    [self.view endEditing:YES];
    int i;
    
    [titleArray addObject:_txtTitle1.text];
    [titleArray addObject:_txtTitle2.text];
    [titleArray addObject:_txtTitle3.text];
    
    [descriptionArray addObject:_vtxtDescription1.text];
    [descriptionArray addObject:_vtxtDescription2.text];
    [descriptionArray addObject:_vtxtDescription3.text];
    
    [APPDELEGATE showLoadingView:self];
    
    for(i = 0 ; i < titleArray.count ; i++){
        
        if([titleArray[i]  isEqual: @""]){
            
            break;
        }
        
        NSString * url = [NSString stringWithFormat:@"%@%@", SERVER_URL, REQ_POSTITEM];
        NSDictionary *params = @{RES_ID  : [NSNumber numberWithInt:APPDELEGATE.Me._idx],
                                 RES_TITLE : titleArray[i],
                                 RES_DESCRIPTION :descriptionArray[i]
                                 };
        
        
        // NSInteger ss=APPDELEGATE.Me._idx;
        NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] multipartFormRequestWithMethod:@"POST" URLString:url parameters:params constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
            [formData appendPartWithFileURL:[NSURL fileURLWithPath:arrPhotoPath[i]] name:@"file" fileName:@"filename.jpg" mimeType:@"image/jpg" error:nil];
        } error:nil];
        
        AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
        
        
        
        NSURLSessionUploadTask *uploadTask;
        uploadTask = [manager
                      uploadTaskWithStreamedRequest:request
                      progress:nil
                      completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
                          if (error) {
                              
                              NSLog(@"Error: %@", error);
                              
                              [APPDELEGATE hideLoadingView];
                              
                              //[APPDELEGATE hideLoadingView];
                            //  [APPDELEGATE showAlertDialog:nil message:PHOTO_UPLOAD_FAIL positive:ALERT_OK negative:nil sender:self];
                              
                              
                          } else {
                              
                              [APPDELEGATE hideLoadingView];
                              
                              NSLog(@"%@ %@", response, responseObject);
                              
                              //[APPDELEGATE hideLoadingView];
                              
                              int nResultCode = [[responseObject valueForKey:RES_CODE] intValue];
                              
                              if(nResultCode == 0) {
                                  
                                 // [APPDELEGATE showAlertDialog:nil message:PHOTO_UPLOAD_SUCCESS positive:ALERT_OK negative:nil sender:self];
                                  
                                  NSLog(@"All items are uploaded sucessfuly.");
                                  
                                  
                                  [self clearPostedItems];
                                  
                                  
                              } else {
                                  
                                 // [APPDELEGATE showAlertDialog:nil message:PHOTO_UPLOAD_FAIL positive:ALERT_OK negative:nil sender:self];
                              }
                          }
                      }];
        
        [uploadTask resume];

    }
}

- (void) clearPostedItems {
    
    [titleArray removeAllObjects];
    [descriptionArray removeAllObjects];
    
    
    
    self.txtTitle1.text = @"";
    self.txtTitle2.text = @"";
    self.txtTitle3.text = @"";

    self.vtxtDescription1.text = @"";
    self.vtxtDescription2.text = @"";
    self.vtxtDescription3.text = @"";
    
    _photoPath1 = @"";
    _photoPath2 = @"";
    _photoPath3 = @"";
    
    [arrPhotoPath removeAllObjects];

}
- (IBAction)viewTapped:(id)sender {
    [self.view endEditing:YES];
}


@end
