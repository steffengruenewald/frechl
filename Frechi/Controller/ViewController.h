//
//  ViewController.h
//  Frechi
//
//  Created by master on 8/29/16.
//  Copyright © 2016 master. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "MyItemViewController.h"

@protocol GoToMyItemViewControllerDelegate <NSObject>

@optional

- (void)reloadMyItemViewController;

// ... other methods here

@end

@interface ViewController : UIViewController 

@property (nonatomic, strong) NSString *selectedImageURL;

@property (nonatomic , strong) NSString *selectedUserName;

@property (nonatomic , strong) NSString *selectedTitle;

@property int selectedUserId;

@property (nonatomic) int selectItemId;

@property (nonatomic, strong) NSString *myItemImageURL;

@property (nonatomic , strong) NSString *userItemImageURL;

@property(nonatomic , strong) NSString *gotoPage;

//@property(nonatomic , strong) MyItemViewController *myItem;

@property (nonatomic, strong) id <GoToMyItemViewControllerDelegate> delegate;


- (void) gotoMarket;
- (void) gotoMyItem;
- (void) gotoMakeDeal;
- (void) gotoInvite;
- (void) gotoInventory;
- (void) gotoMike;
- (void) gotoCongrate;
-(void) gotoDashBoard;
@end
