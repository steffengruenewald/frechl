//
//  DashBoardViewController.m
//  Frechi
//
//  Created by master on 8/31/16.
//  Copyright © 2016 master. All rights reserved.
//

#import "DashBoardViewController.h"
#import "ViewController.h"

@interface DashBoardViewController(){
    
}

@end

@implementation DashBoardViewController

-(void) viewDidLoad{
    
    [super viewDidLoad];
}
-(void) viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
}

-(void) viewWillDisappear:(BOOL)animated{
    
    [super viewWillDisappear:animated];
}

- (IBAction)goToMarket:(id)sender {
    
    [self.rootVC gotoMarket];

}

- (IBAction)goToMyItem:(id)sender {
    
    [self.rootVC gotoMyItem];
}

- (IBAction)goToInvite:(id)sender {
    
    [self.rootVC gotoInvite];
    
}
- (IBAction)goToInventory:(id)sender {
    
    [self.rootVC gotoInventory];
}
@end
