//
//  DashBoardViewController.h
//  Frechi
//
//  Created by master on 8/31/16.
//  Copyright © 2016 master. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ViewController.h"

@interface DashBoardViewController : UIViewController

@property (nonatomic, strong) ViewController * rootVC;

@end
