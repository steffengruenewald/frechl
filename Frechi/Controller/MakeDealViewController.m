//
//  MakeDealViewController.m
//  Frechi
//
//  Created by master on 9/5/16.
//  Copyright © 2016 master. All rights reserved.
//

#import "MakeDealViewController.h"
#import "DirectoryCell.h"
#import "AppDelegate.h"
#import "CommonUtils.h"
#import "UserEntity.h"
#import "ViewController.h"

@interface MakeDealViewController ()<UITableViewDelegate , UITableViewDataSource>{
    
    NSMutableArray *myItemList;
    
}
@property (weak, nonatomic) IBOutlet UIImageView *imvItemImage;

@property (weak, nonatomic) IBOutlet UITableView *tblMyItemList;

@property (weak, nonatomic) IBOutlet UILabel *lblUserName_Title;

@end

@implementation MakeDealViewController

- (void) viewDidLoad{
    
    [super viewDidLoad];
    [self initView];
}

- (void) viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear: animated];
}

- (void) viewWillDisappear:(BOOL)animated{
    
    [super viewWillDisappear:animated];
}

- (void) initView {
    
    
    
   // [self getMyItem];
}

- (void) updateState {
    
    [_imvItemImage setImageWithURL:[NSURL URLWithString:_userImageUrl]];
    _lblUserName_Title.text = [NSString stringWithFormat:@"%@ -  %@" , _userName , _userTitle];
    
}

- (void) getMyItem {
    
   
    
    if (APPDELEGATE.Me._idx > 0)
    {
        [APPDELEGATE showLoadingView:self];
        myItemList = [[NSMutableArray alloc] init];
        NSString *url = [NSString stringWithFormat:@"%@%@/%d", SERVER_URL, REQ_MTITEM,APPDELEGATE.Me._idx];
        NSLog(@"%d" , APPDELEGATE.Me._idx);
        NSLog(@"%@",url);
        
        NSString *  escapedURL = [url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        AFHTTPSessionManager * manager = [AFHTTPSessionManager manager];
        [manager GET:escapedURL parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            
            // success response
            // parse the data from server
            int nResult_Code = [[responseObject valueForKey:RES_CODE] intValue];
            if(nResult_Code == CODE_SUCCESS) {
                
                NSLog(@"%d",nResult_Code);
                //UserEntity *itemEntity = [[UserEntity alloc] init];
                NSArray *jsonItems = [responseObject objectForKey:RES_MYITEMS];
                for(NSDictionary * _dict in jsonItems){
                    
                    UserEntity *itemEntity = [[UserEntity alloc] init];
                    itemEntity._itemId = [[_dict valueForKey:RES_ITEMID] intValue];
                    itemEntity._title = [_dict valueForKey:RES_TITLE];
                    itemEntity._description = [_dict valueForKey:RES_DESCRIPTION];
                    itemEntity._itemImageUrl =  [_dict valueForKey:RES_ITEMIMAGE_URL];
                    [myItemList addObject:itemEntity];
                    
                    
                }
                
                
                [self.tblMyItemList reloadData];
                [APPDELEGATE hideLoadingView];
            }
            else if(nResult_Code == CODE_UNREGUSER)
            {
                
                // [APPDELEGATE showAlertDialog:nil message:UNREGISTERED_USER positive:ALERT_OK negative:nil sender:self];
                
                // [self performSegueWithIdentifier:@"SequeLoginToCategory" sender:nil];
                
                [APPDELEGATE hideLoadingView];

            }
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            
            // failure
            NSLog(@"Error: %@", error);
            
            [APPDELEGATE hideLoadingView];
            //[APPDELEGATE showAlertDialog:nil message:CONN_ERROR positive:ALERT_OK negative:nil sender:self];
        }];
    }
}

#pragma mark - UITableViewDataSource
// ---------------------------------------------------------------------------
- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView {
    
    // return the number of sections
    return 1;
}


- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // return APPDELEGATE.MyItems.count;
    return myItemList.count;
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    //    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"DirectoryCell" forIndexPath:indexPath];
    
    static NSString * cellIdentifier =   @"DirectoryCell";
    DirectoryCell *cell = (DirectoryCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    [cell setUserModel:myItemList[indexPath.row]];
    return cell;
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 100;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (APPDELEGATE.Me._idx > 0)
    {
        [APPDELEGATE showLoadingView:self];
        UserEntity *changeEntity = [[UserEntity alloc] init];
        changeEntity = (UserEntity *)myItemList[indexPath.row];
        
        NSString *url = [NSString stringWithFormat:@"%@%@/%d/%d/%d/%d", SERVER_URL, REQ_SENDCHANGE,APPDELEGATE.Me._idx , changeEntity._itemId, _userId,_userItemId];
        
        NSLog(@"%@",url);
        
        NSString *  escapedURL = [url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        AFHTTPSessionManager * manager = [AFHTTPSessionManager manager];
        [manager GET:escapedURL parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            [APPDELEGATE hideLoadingView];
            // success response
            // parse the data from server
            int nResult_Code = [[responseObject valueForKey:RES_CODE] intValue];
            if(nResult_Code == CODE_SUCCESS) {
                
                _rootVC.myItemImageURL = changeEntity._itemImageUrl;
                _rootVC.userItemImageURL = _userImageUrl;
                
                
                //[self.rootVC gotoMike];
                
                
            }
            else if(nResult_Code == CODE_UNREGUSER)
            {
                
            }
            
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            
            // failure
            NSLog(@"Error: %@", error);
            
              [APPDELEGATE hideLoadingView];
            //[APPDELEGATE showAlertDialog:nil message:CONN_ERROR positive:ALERT_OK negative:nil sender:self];
        }];
    }

    
}




@end

