//
//  MarketViewController.m
//  Frechi
//
//  Created by master on 9/1/16.
//  Copyright © 2016 master. All rights reserved.
//

#import "MarketViewController.h"
#import "DirectoryCell.h"
#import "CommonUtils.h"
#import "UserEntity.h"
#import "ViewController.h"
#import "MakeDealViewController.h"


@interface MarketViewController ()<UITableViewDelegate , UITableViewDataSource>{
    
  //  UserEntity *itemEntity;
    NSMutableArray *itemList;
}
@property (weak, nonatomic) IBOutlet UITableView *tblItemList;

@end

@implementation MarketViewController

-(void) viewDidLoad{
    
    [super viewDidLoad];
    
    //itemEntity = [[UserEntity alloc] init];
    
   
    
}

-(void) viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    [self initView];

    
}

-(void) viewWillDisappear:(BOOL)animated{
    
    [super viewWillDisappear:animated];
}

-(void) initView{
    
    //[self getAllItem];
 
}
-(void) getAllItem{
    
     itemList = [[NSMutableArray alloc] init];
    if(APPDELEGATE.Me._idx > 0){
        [APPDELEGATE showLoadingView:self];
        NSString *url = [NSString stringWithFormat:@"%@%@/%d/%@", SERVER_URL, REQ_GETALLITEMS,APPDELEGATE.Me._idx, APPDELEGATE.Me._schoolName];
        NSLog(@"%d" , APPDELEGATE.Me._idx);
        NSLog(@"%@",url);
        
        NSString *  escapedURL = [url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        AFHTTPSessionManager * manager = [AFHTTPSessionManager manager];
        [manager GET:escapedURL parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            
            // success response
            // parse the data from server
            int nResult_Code = [[responseObject valueForKey:RES_CODE] intValue];
            if(nResult_Code == CODE_SUCCESS) {
                
                NSLog(@"%d",nResult_Code);
                NSArray *jsonItems = [responseObject objectForKey:ALL_ITEMS];
                for(NSDictionary * _dict in jsonItems){
                    
                    UserEntity *itemEntity = [[UserEntity alloc] init];
                    itemEntity._itemId = [[_dict valueForKey:RES_ITEMID] intValue];
                    itemEntity._idx = [[_dict valueForKey:RES_ID] intValue];
                    itemEntity._userName = [_dict valueForKey:RES_USERNAME];
                    itemEntity._title = [_dict valueForKey:RES_TITLE];
                    itemEntity._description = [_dict valueForKey:RES_DESCRIPTION];
                    itemEntity._photoUrl = [_dict valueForKey:RES_USERPHOTOURL];
                    itemEntity._itemImageUrl =  [_dict valueForKey:RES_ITEMIMAGEURL];
                    [itemList addObject:itemEntity];
                    //NSLog(@"%lu", (unsigned long)itemList.count);

                }
                
                
                [self.tblItemList reloadData];
                [APPDELEGATE hideLoadingView];
            }
            else if(nResult_Code == CODE_UNREGUSER)
            {
                
                // [APPDELEGATE showAlertDialog:nil message:UNREGISTERED_USER positive:ALERT_OK negative:nil sender:self];
                
                // [self performSegueWithIdentifier:@"SequeLoginToCategory" sender:nil];
                
                [APPDELEGATE hideLoadingView];
            }
          
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        // failure
        NSLog(@"Error: %@", error);
        
          [APPDELEGATE hideLoadingView];
        //[APPDELEGATE showAlertDialog:nil message:CONN_ERROR positive:ALERT_OK negative:nil sender:self];
    }];
    }
    
}
// ---------------------------------------------------------------------------
#pragma mark - UITableViewDataSource
// ---------------------------------------------------------------------------
- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView {
    
    // return the number of sections
    return 1;
}


- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return itemList.count;
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
     //DirectoryCell * cell = (DirectoryCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    static NSString * cellIdentifier =   @"DirectoryCell";
    DirectoryCell *cell = (DirectoryCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    [cell setUserModel:itemList[indexPath.row]];
    
    return cell;
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UserEntity *entity = itemList[indexPath.row];
    _rootVC.selectedImageURL = entity._itemImageUrl;
    _rootVC.selectedUserName = entity._userName;
    _rootVC.selectedUserId = entity._idx;
    _rootVC.selectedTitle = entity._title;
    _rootVC.selectItemId = entity._itemId;
   
    [self.rootVC gotoMakeDeal];
    
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 110;
}



@end
