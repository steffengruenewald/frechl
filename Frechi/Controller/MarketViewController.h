//
//  MarketViewController.h
//  Frechi
//
//  Created by master on 9/1/16.
//  Copyright © 2016 master. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ViewController.h"

@interface MarketViewController : UIViewController

@property (nonatomic, strong) ViewController * rootVC;

-(void) getAllItem;
@end
