//
//  UserEntity.h
//  Frechi
//
//  Created by master on 9/1/16.
//  Copyright © 2016 master. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserEntity : NSObject

@property (nonatomic) int _idx;
@property (nonatomic, strong) NSString *_userName;
@property (nonatomic, strong) NSString *_photoUrl;
@property (nonatomic, strong) NSString *_schoolName;
@property (nonatomic) long _toggleStatus;

@property (nonatomic) int _itemId;
@property (nonatomic , strong) NSString *_title;
@property (nonatomic , strong) NSString *_description;
@property (nonatomic , strong) NSString *_itemImageUrl;


@end
