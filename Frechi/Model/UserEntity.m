//
//  UserEntity.m
//  Frechi
//
//  Created by master on 9/1/16.
//  Copyright © 2016 master. All rights reserved.
//

#import "UserEntity.h"

@implementation UserEntity

@synthesize _idx , _userName,_photoUrl,_schoolName,_toggleStatus;
@synthesize _itemId,_title , _description , _itemImageUrl;

-(instancetype) init{
    
    if(self = [super init])
    {
        _idx = 0;
        _userName = @"";
        _photoUrl = @"";
        _schoolName = @"";
        _toggleStatus = 255;
        
        _itemId = 0;
        _title = @"";
        _description = @"";
        _itemImageUrl = @"";
    
    }
    return self;
}

@end
